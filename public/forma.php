<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Бобух Олег 27/2</title>
  </head>
  <body>
  <p><a id="top"></a></p>
    <header>
        <div class="block">
            <img id="logo" src="https://cdn23.img.ria.ru/images/07e4/03/19/1569130450_0:0:1863:1048_600x0_80_0_0_23b8c30409e35b1b1c647b71d20c6238.png" alt="Логотип" />
        </div>
        <h1>Бобух Олег Владимирович 27/2</h1>
    </header>
  <div class="my-blocks">
    <div id="form">
    <form action="" method="POST">
      <label>
        Ваше имя:<br />
        <input name="name"
        placeholder="Ivan" />
      </label><br />

      <label>
        Ваш email:<br />
        <input name="mail"
        placeholder="test@example.com"
          type="email" />
      </label><br />
    
      <label>
          <p>Ваш год рождения:</p>
          <select name="year"><option value="1985">1985</option>
                              <option value="1986">1986</option>
                              <option value="1987">1987</option>
                              <option value="1988">1988</option>
                              <option value="1989">1989</option>
                              <option value="1990">1990</option>
                              <option value="1991">1991</option>
                              <option value="1992">1992</option>
                              <option value="1993">1993</option>
                              <option value="1994">1994</option>
                              <option value="1995">1995</option>
                              <option value="1996">1996</option>
                              <option value="1997">1997</option>
                              <option value="1998">1998</option>
                              <option value="1999">1999</option>
                              <option value="2000">2000</option>
                              <option value="2001" selected="selected">2001</option>
                              <option value="2002">2002</option>
                              <option value="2003">2003</option>
                              <option value="2004">2004</option>
                              <option value="2005">2005</option>
                              <option value="2006">2006</option>
                              <option value="2007">2007</option>
                              <option value="2008">2008</option>
                              <option value="2009">2009</option>
                              <option value="2010">2010</option>
                              <option value="2011">2011</option>
                              <option value="2012">2012</option>
                              <option value="2013">2013</option>
                              <option value="2014">2014</option>
                              <option value="2015">2015</option>
                              <option value="2016">2016</option>
            </select>
      </label><br />
    
      <label>      
        Ваш пол:<br />
        <input type="radio" checked="checked"
          name="sex" value="M" />
          Мужской
      </label>
      <label>
        <input type="radio"
          name="sex" value="F" />
          Женский
      </label><br />

      <label>    
        Кол-во сданных книг:<br />
        <input type="radio" checked="checked"
          name="kol" value="1" />
          1
      </label>
      <label><input type="radio" 
          name="kol" value="2" />
          2
      </label>
      <label><input type="radio" 
          name="kol" value="3" />
          3
      </label>
      <label><input type="radio"
          name="kol" value="4" />
          более 4
      </label><br />
    
      <label>
                    Виды учебников:
                    <br />
                    <select name="books[]"
                            multiple="multiple">
                            <option value="Reading your mind" selected="selected">
                        <option value="Poems">Стихи</option>
                        <option value="Children's literature" selected="selected">Десткая лит-ра</option>
                        <option value="High school" selected="selected">Старшая школа</option>
                        <option value="Primary school" selected="selected">Начальная школа</option>
                    </select>
                </label><br />
                <label>
                    О любимом авторе:<br />
                    <textarea name="favorite">
	        Мой любимый автор...
	        </textarea>
                </label><br />

                Согласие на продолжение:<br />
                <label>
                    <input type="checkbox" checked="checked"
                           name="check1" />
                    Согласен!!!!
                </label><br />


                <input type="submit" value="Отправить" />
    </form>
  </div>
</div>
<footer>
    (с) Бобух Олег</br>Краснодар, Кубанский Государственный Университет.
</footer>
</body>
</html>
